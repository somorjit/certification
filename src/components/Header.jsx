import React from "react";
import { Link, useLocation } from "react-router-dom";
function Header(props) {
  const { countCartItems } = props;
  const location = useLocation();
  //DESTRUCTURING PATHNAME FROM LOCATION
  const { pathname } = location;
  //SPLIT THE PATH FROM ARRAY
  const splitLocation = pathname.split("/");
  return (
    <header>
      <div className='container'>
        <div className='row'>
          <div className='col-md-4 d-flex align-items-center'>
            <div className='logo'>
              <a href='/shop'>My Shopping</a>
            </div>
          </div>
          <div className='col-md-8 float-right d-flex justify-content-end '>
            <nav>
              <ul>
                {/* Checking the current path name using javascript ternary operator and if true adding active classname to it */}
                <li className={splitLocation[1] === "shop" ? "shop active" : "shop"}>
                  <Link to='/shop'>SHOP</Link>
                </li>
                <li className={splitLocation[1] === "cart-details" ? "cart active" : "cart"}>
                  <Link to='/cart-details'>CART {props.countCartItems ? <span className='count-badge'>{countCartItems}</span> : <span className='count-badge'>0</span>}</Link>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
