import React from "react";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
const Product = (props) => {
  const [products, setProducts] = useState([]);
  const [loader, setLoader] = useState(true);
  const fetchProducts = () => {
    const headerRequest = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      mode: "cors",
      cache: "default",
      SetCookie: "promo_shown=1; SameSite=Lax",
    };
    const URL = "https://run.mocky.io/v3/ff976dfb-405e-4216-baad-1bffbaf8e027";
    fetch(URL, headerRequest)
      .then((response) => response.json())
      .then((result) => {
        //console.log(result);
        setLoader(false);
        setProducts(result);
      });
  };
  useEffect(() => {
    fetchProducts();
  }, []);
  const { cartItems, cartCount, showCart, setcartCount } = props;
  const increaseCardCount = (product) => {
    let newCartCount = { ...cartCount };
    if (cartCount[product.id] && cartCount[product.id] >= 2) {
      newCartCount[product.id] = cartCount[product.id] - 1;
    } else {
      newCartCount[product.id] = 1;
    }
    setcartCount(newCartCount);
  };
  const reduceCardCount = (product) => {
    let newCartCount = { ...cartCount };
    if (cartCount[product.id]) {
      newCartCount[product.id] = cartCount[product.id] + 1;
    } else {
      newCartCount[product.id] = 2;
    }
    setcartCount(newCartCount);
  };
  return (
    <>
      <h2>Product List</h2>
      <div className='products-list-container row'>
        {loader ? (
          <div className='loader'>
            <h2>
              <div className='loadingio-spinner-ball-izi5445lev'>
                <div className='ldio-vej4k2a4k7'>
                  <div> </div>
                </div>
              </div>
            </h2>
          </div>
        ) : null}
        {products.map((product) => (
          <div className='col-md-3' key={product.id}>
            <div className='products-list-container-list'>
              <img className='product-thumbnail' src={product.image} alt={product.name} />
              <div className='products-list-container-details'>
                <h3>{product.name}</h3>
                <p>{product.description.substring(0, 80)}</p>
                <div className='cart-count-wrap'>
                  <div className='qty-label'>Quantity: </div>
                  <button
                    onClick={() => {
                      increaseCardCount(product);
                    }}
                    className={cartCount[product.id] && cartCount[product.id] >= 2 ? "enabled" : "disabled"}>
                    -
                  </button>
                  <p>{cartCount[product.id] || 1}</p>
                  <button
                    className='add-cart-count'
                    onClick={() => {
                      reduceCardCount(product);
                    }}>
                    +
                  </button>
                </div>
                <h4>Price: ₹{cartCount[product.id] ? product.price * cartCount[product.id] : product.price}</h4>
                <button className='add-to-cart' onClick={() => showCart(product)} title='Add to Cart'>
                  Add to Cart
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
      <div className='col-md-12 text-center'>
        {cartItems.length !== 0 ? (
          <Link className='view-cart btn-default' to='/cart-details'>
            Go to Cart
          </Link>
        ) : null}
      </div>
    </>
  );
};

export default Product;
