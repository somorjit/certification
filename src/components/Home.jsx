import React from "react";
import Header from "./Header";
import Cart from "./Cart";
import Product from "./Product";
import { useState, useRef, useEffect } from "react";
import { useLocation } from "react-router-dom";
import Footer from "./Footer";

const Home = (props) => {
  const location = useLocation();
  //DESTRUCTURING PATHNAME FROM LOCATION
  const { pathname } = location;
  //SPLIT THE PATH FROM ARRAY
  const splitLocation = pathname.split("/");
  const [cartItems, setcartItems] = useState([]);
  const [cartCount, setcartCount] = useState({});
  const [showMessageCart, setShowMessageCart] = useState(false);
  const [existingCart, setExistingCart] = useState(false);
  //USE EFFECT TO SHOW MESSAGE ON ADDTOCART
  useEffect(() => {
    setTimeout(function () {
      setShowMessageCart(false);
    }, 3000);
  }, [showMessageCart]);
  //USE EFFECT TO SHOW MESSAGE ON UPDATE CART
  useEffect(() => {
    setTimeout(function () {
      setExistingCart(false);
    }, 3000);
  }, [existingCart]);

  //USE EFFECT TO UPDATE CARTITEMS AND CARTCOUNT
  const currenCartCount = useRef("");
  useEffect(() => {
    currenCartCount.current = cartCount;
  }, [cartItems, cartCount]);
  //ADD TO CART FUNCTION
  const showCart = (product) => {
    const exist = cartItems.find((x) => x.id === product.id);
    if (exist) {
      setcartItems(cartItems.map((x) => (x.id === product.id ? { ...exist, qty: exist.qty + cartCount[product.id] } : x)));
      setExistingCart(true);
    } else {
      setcartItems([...cartItems, { ...product, qty: cartCount[product.id] || 1 }]);
      setShowMessageCart(true);
    }
  };
  //INCREASE CART COUNT ON CART PAGE
  const onAdd = (product) => {
    const exist = cartItems.find((x) => x.id === product.id);
    if (exist) {
      setcartItems(cartItems.map((x) => (x.id === product.id ? { ...exist, qty: exist.qty + 1 } : x)));
    } else {
      setcartItems([...cartItems, { ...product, qty: cartCount[product.id] }]);
    }
  };
  //DECREASE CART COUNT ON CART PAGE
  const onRemove = (product) => {
    let newCartItems = [...cartItems];
    let productIndex;
    let newProduct;
    cartItems.forEach((item, index) => {
      if (item.id === product.id) {
        newProduct = item;
        productIndex = index;
      }
    });
    newProduct.qty -= 1;
    newCartItems[productIndex] = newProduct;
    setcartItems(newCartItems);
  };
  //DELETE PRODUCT FROM CART FUNCTION
  const onDelete = (product) => {
    let newCartItems = [...cartItems];
    let productIndex;
    cartItems.forEach((item, index) => {
      if (item.id === product.id) {
        productIndex = index;
      }
    });
    newCartItems.splice(productIndex, 1);
    setcartItems(newCartItems);
  };
  return (
    <>
      <Header countCartItems={cartItems.length} />
      <div className='container main-content'>
        {splitLocation[1] === "shop" ? <Product cartItems={cartItems} cartCount={cartCount} setcartCount={setcartCount} showCart={showCart} onAdd={onAdd} onRemove={onRemove} /> : null}
        {splitLocation[1] === "cart-details" ? <Cart cartItems={cartItems} onAdd={onAdd} showCart={showCart} onRemove={onRemove} onDelete={onDelete}></Cart> : null}
      </div>
      {showMessageCart ? (
        <div className='shopping-cart-update-message'>
          <p>Product has been added to cart</p>
        </div>
      ) : null}
      {existingCart ? (
        <div className='shopping-cart-update-message'>
          <p>Product count updated</p>
        </div>
      ) : null}

      <Footer />
    </>
  );
};
export default Home;
