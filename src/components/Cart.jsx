import React from "react";
import { Link } from "react-router-dom";

const Cart = (props) => {
  const { cartItems, onAdd, onRemove, onDelete } = props;
  const itemsPrice = cartItems.reduce((a, c) => a + c.qty * c.price, 0);
  const GST = itemsPrice * 0.14;
  const ShippingCost = itemsPrice > 2000 ? 0 : 20;
  const totalPrice = itemsPrice + GST + ShippingCost;
  return (
    <>
      <div className='back-to-top'>
        <Link to='/shop'>Back to Shop &#8617;</Link>
      </div>
      <div className='shopping-cart-wrapper'>
        <div className='shopping-cart-container'>
          <div className='overlay-backdrop'></div>
          <div className='container'>
            <div className='row'>
              <h2>Shopping Cart {cartItems.length === 0 && <span>is Empty</span>}</h2>
            </div>
            {cartItems.map((item) => (
              <div className='cart-items-list-products' key={item.id}>
                <div className='row'>
                  <div className='col-md-5'>
                    <img src={item.image} alt={item.name} />
                    <h3>{item.name}</h3>
                  </div>
                  <div className='col-md-2 cart-counts'>
                    {item.qty === 1 ? (
                      <button className='remove-product'>-</button>
                    ) : (
                      <button onClick={() => onRemove(item)} className='remove-product'>
                        -
                      </button>
                    )}{" "}
                    <span>{item.qty}</span>
                    <button onClick={() => onAdd(item)} className='add-product'>
                      +
                    </button>
                  </div>
                  <div className='col-md-3 cart-price'>
                    <p className='text-right'>
                      {item.price} X {item.qty} = ₹{(item.price * item.qty).toFixed(2)}
                    </p>
                  </div>
                  <div className='col-md-2 delete-container'>
                    <button onClick={() => onDelete(item)} className='btn-default remove-product'>
                      Delete
                    </button>
                  </div>
                </div>
              </div>
            ))}
            {cartItems.length !== 0 && (
              <>
                <div className='cart-items-list-pricing'>
                  <div className='row'>
                    <div className='col-md-10'>Items Price</div>
                    <div className='col-md-2'>
                      <p className='text-right'>₹{itemsPrice.toFixed(2)}</p>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-md-10'>GST</div>
                    <div className='col-md-2 text-right'>
                      <p className='text-right'>₹{GST.toFixed(2)}</p>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-md-10'>Shipping Price (Free Above 2000/- Total)</div>
                    <div className='col-md-2 text-right'>
                      <p className='text-right'>₹{ShippingCost.toFixed(2)}</p>
                    </div>
                  </div>
                </div>
                <div className='cart-items-list-total'>
                  <div className='row'>
                    <div className='col-md-10'>
                      <strong>Total Price</strong>
                    </div>
                    <div className='col-md-2 text-right'>
                      <strong>
                        <p className='text-right'>₹{totalPrice.toFixed(2)}</p>
                      </strong>
                    </div>
                  </div>
                </div>
                <div className='cart-items-list-cart-payment'>
                  <div className='row'>
                    <div className='col-md-3 offset-md-9'>
                      <button className='btn-default' onClick={() => alert("Redirect to Checkout/Payment Page")}>
                        Pay Now
                      </button>
                    </div>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
};
export default Cart;
