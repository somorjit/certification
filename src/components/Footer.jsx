import React from "react";
import { NavLink, Link, useLocation } from "react-router-dom";

const Footer = () => {
  //assigning location variable
  const location = useLocation();

  //destructuring pathname from location
  const { pathname } = location;

  //Javascript split method to get the name of the path in array
  const splitLocation = pathname.split("/");
  return (
    <footer>
      <div className='container'>
        <div className='row'>
          <div className='col-md-6 d-flex align-items-center'>
            <p className='copyright'>&copy; 2022. All Rights Reserved.</p>
          </div>
          <div className='col-md-6 d-flex flex-row justify-content-end text-right'>
            <p>
              <a href='#'>Terms and Conditions</a> | <a href='#'>Privacy Policy</a>
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
