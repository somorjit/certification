import React from "react";
import Footer from "./Footer";
import Header from "./Header";
import { useLocation, useParams } from "react-router-dom";

const ErrorPage = (props) => {
  const location = useLocation();
  return (
    <>
      <Header />
      <div className='main-content'>
        <div className='container'>
          <p>{location.pathname.toLocaleUpperCase().split("/")} page not Found!</p>
        </div>
      </div>
      <Footer />
    </>
  );
};
export default ErrorPage;
