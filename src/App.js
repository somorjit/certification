import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./components/Home";
import ErrorPage from "./components/Error";
function App() {
  return (
    <main>
      <Router>
        <Routes>
          <Route path='/shop' element={<Home />} />,
          <Route path='/cart-details' element={<Home />} />,
          <Route path='/*' element={<ErrorPage />} />
        </Routes>
      </Router>
    </main>
  );
}

export default App;
